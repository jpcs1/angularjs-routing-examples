angular
    .module('ordersapp')
    .factory('OrdersService', function () {
        var instance = {};
        var orders = [
            'Order #123321 placed on 2014-08-15. Order detail: Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'Order #125644 placed on 2014-08-15. Order detail: Donec porta orci vitae magna scelerisque tristique.',
            'Order #892301 placed on 2014-08-15. Order detail: Nulla mattis odio eget eros lacinia, eget volutpat sapien posuere.',
            'Order #981246 placed on 2014-08-15. Order detail: Quisque scelerisque erat in sem commodo luctus.'
        ];

        instance.getOrders = function(){
            return orders;
        }

        instance.addOrder = function(data) {
            orders.push(
                    'Order ' + parseInt(Math.random()*1000000) + ' placed on ' +
                        (new Date()).toLocaleString() + '. Order detail: ' +  data
            )

            return orders
        }

        instance.getOrder = function(idx) {
            return orders[idx];
        }

        return instance;
    });